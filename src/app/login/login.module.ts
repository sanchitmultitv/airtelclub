import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { AnimateComponent } from './animate/animate.component';
import { NgxSimpleCountdownModule } from 'ngx-simple-countdown';

@NgModule({
  declarations: [LoginComponent, AnimateComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    NgxSimpleCountdownModule
  ]
})
export class LoginModule { }
