import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
agenda1=[];
// url="https://virtualapi.multitvsolution.com/exhibitions/165_agenda_60a21dd9252fc.pdf"
  constructor(private _fd:FetchDataService) { }

  ngOnInit(): void {
    this.getPdf();
  }

  getPdf(){
    this._fd.activeAudi().subscribe(res => {
      // console.log(res.result[0].agenda,'heeelo')
      this.agenda1 = res.result[0].agenda[0];
      console.log(this.agenda1,'HELLO');
    });
  }
}
