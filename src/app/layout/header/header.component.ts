import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { SteupServiceService } from 'src/app/services/steup-service.service';
import { MenuItems } from '../shared/menu.items';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menuItems = MenuItems;
  constructor(public router: Router, private _analytics: SteupServiceService,private auth: AuthService, private _fd: FetchDataService) { }

  ngOnInit(): void {
  }
  exit(){ 
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    let signify_userid = JSON.parse(localStorage.getItem('getdata')).id;
    const formsData = new FormData();
      formsData.append('user_id', signify_userid);
      formsData.append('flag', '0');
      const newdata = new FormData();
      newdata.append('user_id', signify_userid);
      newdata.append('status','0');
    this._fd.handraise(formsData).subscribe(res=>{
        console.log(res);
    });
    this._fd.signify_update(formsData).subscribe(res=>{
      
    })
    this._fd.handlogout(newdata).subscribe(res=>{

    });
    this.auth.logout(user_id).subscribe(res => {
      this.router.navigate(['/login']);
      localStorage.clear();
    });


    // localStorage.removeItem('virtual');
    // this.router.navigate(['/login']);
  }
  stepup(action){
    this._analytics.stepUpAnalytics(action);
  }
}
