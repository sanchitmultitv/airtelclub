import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatService } from '../services/chat.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {
  livemsg = false;
  routeNoti = false;
  getMsg
  untillMsg = false;
  rapidMsg:any;
  allmsg
  data2 = 'eventhall'
  data3 = 'eventhall2'
  data4 = 'eventhall3'
  data5 = 'photobooth'
  token = 'toujeo-179';
  showVideo = false;
  networkingLounge = false;
  receptionEnd = false;
  auditoriumLeft = false;

  constructor(public router: Router, private ad: ActivatedRoute, private chat: ChatService) { }

  ngOnInit(): void { 
    this.chat.getconnect('6059ecd30air3');
  this.chat.getMessages().subscribe((data => {
       console.log('data',data);
       let checkData = data.split('*');
       let mymail = localStorage.getItem('myemail');
      //  alert(checkData)
       if(checkData[0] == mymail && checkData[1]!='exit'){
         localStorage.setItem('user_key', checkData[1]);
        var timeleft = 10;
        var downloadTimer = setInterval(() =>{
      if(timeleft <= 0){
        clearInterval(downloadTimer);
        document.getElementById("countdown").innerHTML = "";
        document.getElementById("countdown").style.display = "none";
        this.routing();
      } else {
        document.getElementById("countdown").style.display = "block";
        document.getElementById("countdown").innerHTML ="You are going live in " +timeleft + " seconds.";
        
      }
      console.log(timeleft);
      timeleft -= 1;
    }, 1000);
       }
       
      //10 seconds timer 
     
       
    //     this.msg = data;
       
    //    let dta = this.msg.split("start_live_");
    //    this.msg = dta[1];
    //   //  console.log('msg',this.msg,dta);
      
    //  if (data == 'start_live') {
    //  this.liveMsg = true;
    //  }
    //  document.getElementById("this.liveMsg");
    

    //  if (data == 'stop_live') {
    //    this.liveMsg = false;
     
    //  }
     
    })); 
    this.chat.getconnect('toujeo-179');
    this.chat.getSocketMessages('toujeo-179').subscribe((res:any)=>{
      let check = res.split('_');
      if(check[0] == 'start' && check[1] == 'live'){
          this.untillMsg = true;
          this.rapidMsg = check[2];
        // alert(this.rapidMsg)

      }
      if(check[0] == 'stop' && check[1] == 'live'){
        this.untillMsg = false;
        // this.rapidMsg == check[2];
    }
    if(res == 'start_audio'){
    this.router.navigate(['/eventhall']);
      // this.untillMsg = false;
      // this.rapidMsg == check[2];
    }
      console.log('testing', res);
    });
  }
  routing(){
    //alert('hel');
    this.router.navigate(['/mainCall']);
  }
  ngAfterViewInit() {
    // this.countdownTimer();
    this.chat.getconnect(this.token);
    this.chat.getMessages().subscribe((data => {
      // console.log('layout', data);
      if (data === 'start_live') {
        this.livemsg = true;
        setTimeout(() => {
          this.router.navigate(['eventhall']);
        }, 3000);
      }
      if (data === 'stop_live') {
        this.livemsg = false;
      }
      // custom_live_auditorium
      console.log('layout', data);

      let check = data.split('_');
      if (check[0] == 'custom' && check[1] == 'live' && check[2] == 'awards') {
        this.allmsg = check[2];
        this.getMsg = check[3]
        this.routeNoti = true;
      }
      if (check[0] == 'custom' && check[1] == 'live' && check[2] == 'stage') {
        this.allmsg = check[2];
        this.getMsg = check[3]
        this.routeNoti = true;
      }
      if (check[0] == 'custom' && check[1] == 'live' && check[2] == 'qna') {
        this.allmsg = check[2];
        this.getMsg = check[3]
        this.routeNoti = true;
      }
      if (check[0] == 'custom' && check[1] == 'live' && check[2] == 'lobby') {
        this.allmsg = check[2];
        this.getMsg = check[3]
        this.routeNoti = true;
        // this.router.navigate(['/lobby']);


      }
      if (check[0] == 'stop' && check[1] == 'custom') {

        this.routeNoti = false;
      }

    }));
  }
  gotoRoute(data) {
    // alert(data)
    // if(data == 'auditorium'){

    //     this.networkingLounge = true;
    //     this.showVideo = true;
    //     let vid: any = document.getElementById(data);
    //     vid.play();
    //   this.routeNoti = false;

    // }
    if(data == 'lobby'){
      this.routeNoti = false;
      this.router.navigate(['/lobby']);
    }

    if (data == 'awards') {
      this.routeNoti = false;
      // alert(data)
      this.networkingLounge = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
      // this.router.navigate([`/${this.data2}`]);
    }
    if (data == 'qna') {
      this.routeNoti = false;
      // alert(data)
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
      // this.router.navigate([`/${this.data3}`]);
    }
    if (data == 'stage') {
      this.routeNoti = false;
      // alert(data)
      this.receptionEnd = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
      // this.router.navigate([`/${this.data4}`]);
    }
  }
  gotoNetworkingLoungeOnVideoEnd() {
    // alert("HELLo")
    this.router.navigate(['/eventhall2']);

    this.networkingLounge = false;

    this.showVideo = false;
  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/eventhall3']);

    this.auditoriumLeft = false;

    this.showVideo = false;
  }
  receptionEndVideo() {
    this.router.navigate(['/eventhall']);

    this.receptionEnd = false;

    this.showVideo = false;
  }
  // countdownTimer(){
  //   var countDownDate = new Date("Mar 15, 2021 16:56:25").getTime();

  //   // Update the count down every 1 second
  //   var x = setInterval(()=> {
  //     var now = new Date().getTime();
  //     var distance = countDownDate - now;
  //     if (distance < 0) {
  //       clearInterval(x);
  //       this.router.navigate(['eventhall']);
  //     }
  //   }, 1000);
  // }
}
