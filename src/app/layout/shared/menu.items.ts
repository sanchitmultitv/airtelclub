export const MenuItems = [
    {
        path: '/lobby',
        icon: 'assets/font/lobby.png',
        name: 'lobby',
        analytics: 'click_lobby'
    },
    {
        path: '/breakout-one',
        icon: 'assets/font/agenda.png',
        name: 'memory wall',
        analytics: 'click_memorywall'
    },
    // { 
    //     datatarget: 'speakersModal',
    //     icon: 'assets/ipf/icons/new/3.png',
    //     name: 'speaker profile',
    //     analytics: 'click_speakerprofile'
    // },
    // {
    //     datatarget: 'feedbackModal',
    //     icon: 'assets/ipf/icons/new/9.png',
    //     name: 'Agenda',
    //     analytics: 'click_agenda'
    // },
    {
        path: '/eventhall',
        icon: 'assets/font/auditorium.png',
        name: 'auditorium',
        analytics: 'auditorium_205004'
    },
    {
        path: '/photobooth',
        icon: 'assets/font/camera.png',
        name: 'photobooth',
        analytics: 'click_photobooth'
    },
    {
        path: '/meetings',
        icon: 'assets/font/rennovate.png',
        name: 'games',
        analytics: 'click_games'
    },
    // {
    //     datatarget: 'agendaModal',
    //     icon: 'assets/q.png',
    //     name: 'q&a',
    //     analytics: 'click_breakout'
    // },
    // {
    //     path: '/breakout-two',
    //     icon: 'assets/ipf/icons/new/7.png',
    //     name: 'Breakout 2',
    //     analytics: 'click_engangementzone'
    // },
    {
        datatarget: 'helpDeskModal',
        icon: 'assets/font/support.png',
        name: 'help desk',
        analytics: 'click_helpdesk'
    },
    
    {
        path: '/login',
        icon: 'assets/font/logout.png',
        name: 'exit',
        analytics: 'click_logout'
    },
];