import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaincallComponent } from './maincall.component';

describe('MaincallComponent', () => {
  let component: MaincallComponent;
  let fixture: ComponentFixture<MaincallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaincallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaincallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
