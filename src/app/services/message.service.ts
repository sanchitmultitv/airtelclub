import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private subject = new Subject<any>();
  private subjectNotification = new Subject<any>();

    sendMessage(message: string) {
        this.subject.next({ text: message });
    }

    clearMessages() {
        this.subject.next();
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
    sendNotification(msg:string){
      this.subjectNotification.next({text:msg});
    }
    clearNotification(){
      this.subjectNotification.next();
    }
    getNotification():Observable<any>{
      return this.subjectNotification.asObservable();
    }
}
