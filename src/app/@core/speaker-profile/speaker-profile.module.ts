import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpeakerProfileRoutingModule } from './speaker-profile-routing.module';
import { SpeakerProfileComponent } from './speaker-profile.component';


@NgModule({
  declarations: [SpeakerProfileComponent],
  imports: [
    CommonModule,
    SpeakerProfileRoutingModule
  ]
})
export class SpeakerProfileModule { }
