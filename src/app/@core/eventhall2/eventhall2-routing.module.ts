import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Eventhall2Component } from './eventhall2.component';

const routes: Routes = [{ path: '', component: Eventhall2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Eventhall2RoutingModule { }
