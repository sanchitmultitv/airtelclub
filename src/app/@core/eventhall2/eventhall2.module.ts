import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Eventhall2RoutingModule } from './eventhall2-routing.module';
import { Eventhall2Component } from './eventhall2.component';


@NgModule({
  declarations: [Eventhall2Component],
  imports: [
    CommonModule,
    Eventhall2RoutingModule
  ]
})
export class Eventhall2Module { }
