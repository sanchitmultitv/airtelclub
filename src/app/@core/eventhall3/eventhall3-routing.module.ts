import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Eventhall3Component } from './eventhall3.component';

const routes: Routes = [{ path: '', component: Eventhall3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Eventhall3RoutingModule { }
