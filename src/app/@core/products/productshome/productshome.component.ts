import { Component, OnInit } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-productshome',
  templateUrl: './productshome.component.html',
  styleUrls: ['./productshome.component.scss']
})
export class ProductshomeComponent implements OnInit {
  img:any;
  result:any;
  cx:any;
  cy:any;
  lens:any = document.createElement("DIV");

  constructor() { }

  movement(imgid, resultid){
    let img:any = document.getElementById(imgid);
    let result:any = document.getElementById(resultid);
    let lens:any = document.createElement("DIV");
    lens.setAttribute('class', 'img-zoom-lens');
    img.parentElement.insertBefore(lens, img)
    let cx = result.offsetWidth / lens.offsetWidth;
    let cy = result.offsetHeight / lens.offsetHeight;
    result.style.backgroundImage = "url('" + img.src + "')";
    result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
    console.log('eef', img.src, cx, cy, img.width);
    let moveLens:any = (e) => {
      var pos, x, y;
      e.preventDefault();
      pos = getCursorPos(e);
      x = pos.x - (lens.offsetWidth / 2);
      y = pos.y - (lens.offsetHeight / 2);
      if (x > img.width - lens.offsetWidth) { x = img.width - lens.offsetWidth; }
      if (x < 0) { x = 0; }
      if (y > img.height - lens.offsetHeight) { y = img.height - lens.offsetHeight; }
      if (y < 0) { y = 0; }
      lens.style.left = x + "px";
      lens.style.top = y + "px";
      result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
    }
    let getCursorPos = (e) => {
      var a, x = 0, y = 0;
      e = e || window.event;
      /*get the x and y positions of the image:*/
      a = img.getBoundingClientRect();
      /*calculate the cursor's x and y coordinates, relative to the image:*/
      x = e.pageX - a.left;
      y = e.pageY - a.top;
      /*consider any page scrolling:*/
      x = x - window.pageXOffset;
      y = y - window.pageYOffset;
      return { x: x, y: y };
    }
  }  
  ngOnInit(): void {
  }
  
}
