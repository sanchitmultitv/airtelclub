import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ProductslistComponent } from './productslist/productslist.component';
import { ProductshomeComponent } from './productshome/productshome.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestingComponent } from './testing/testing.component';
import { BtnDirective } from './testing/directives/btn.directive';


@NgModule({
  declarations: [ProductsComponent, ProductslistComponent, ProductshomeComponent, TestingComponent, BtnDirective],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ProductsModule { }
