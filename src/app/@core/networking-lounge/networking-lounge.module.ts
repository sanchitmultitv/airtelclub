import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NetworkingLoungeRoutingModule } from './networking-lounge-routing.module';
import { NetworkingLoungeComponent } from './networking-lounge.component';
import { LoungeChatComponent } from './lounge-chat/lounge-chat.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [NetworkingLoungeComponent, LoungeChatComponent],
  imports: [
    CommonModule,
    NetworkingLoungeRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class NetworkingLoungeModule { }
