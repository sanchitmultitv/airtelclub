import { Component, OnInit } from '@angular/core';
import { SteupServiceService } from 'src/app/services/steup-service.service';
declare var $: any;

@Component({
  selector: 'app-breakout',
  templateUrl: './breakout.component.html',
  styleUrls: ['./breakout.component.scss']
})
export class BreakoutComponent implements OnInit {

  constructor(private _analytics: SteupServiceService) { }
  videosource;

  ngOnInit(): void {
  } 
  
  playGrowBySharingVideo(video) {
    this.videosource = video;
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }
  closeModalVideo() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }

  stepup(action) {
    this._analytics.stepUpAnalytics(action);
  }
}
