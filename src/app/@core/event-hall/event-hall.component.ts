import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-event-hall',
  templateUrl: './event-hall.component.html',
  styleUrls: ['./event-hall.component.scss']
})
export class EventHallComponent implements OnInit {
  player:any;
  like = false;
  flag:any = 0;
  imag:any;
  constructor(private router: Router, private _fd:FetchDataService,private locationStrategy: LocationStrategy) { }

  ngOnInit(): void {
    history.pushState(null, null, location.href);
    this.locationStrategy.onPopState(() => {
      history.pushState(null, null, location.href);
    })
    let event_id = 179;
    // let event_id = 179;
    this._fd.getPlayerUrl(event_id).subscribe((res:any)=>{
      let stream = res.result[0]['stream'];
      this.playVideo(stream);
    });
  }
  playVideo(stream) {
    var playerElement = document.getElementById("player-wrapper");
    this.player = new Clappr.Player({
      parentId: 'player-wrapper',
      source: stream,
      poster: 'assets/fasenra/poster.png',
      maxBufferLength: 30,
    //   height: '62.3%',
    // width: '56.3%',
      width: '100%',
      height: '100%',
      autoPlay: true,
      loop: true,
      // width: window.innerWidth*.5781,
      // height: window.innerWidth*.5707/16*9,
      hideMediaControl: true,
      hideVolumeBar: true,
      hideSeekBar: true,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);    
  }
  likeopen(data){
    if(data =='like'){
      this.imag = 'assets/fasenra/like.png'
    }
    if(data =='claps'){
      this.imag = 'assets/fasenra/claps.png'
    }
    if(data =='heart'){
      this.imag = 'assets/fasenra/heart.png'
    }
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }
  toggleHandRaise(){
    if(this.flag == 0){
      this.flag =1;
      
    }
    else{
      this.flag = 0;
      
    }
    this.handraise();
  }
  handraise(){
    let signify_userid = JSON.parse(localStorage.getItem('getdata')).id;
    const formsData = new FormData();
    formsData.append('user_id', signify_userid);
    formsData.append('flag', this.flag);
    console.log(formsData);
this._fd.handraise(formsData).subscribe(res=>{
  console.log(res);
})

  }

  gotoChat(){
    this.router.navigate(['/eventhall/chat']);
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  clapping(){
    let clap: any = document.getElementById('myAudioClap');
    clap.play();
  }
  whistling(){
    let whistle: any = document.getElementById('myAudioWhistle');
    whistle.play();
  }
  // @HostListener('window:resize', ['$event']) onResize(event) {
  //   this.player.resize({ width: window.innerWidth*.5781, height: window.innerWidth*.5707/16*9 });
  // }
}
