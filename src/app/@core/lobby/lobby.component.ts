import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { SteupServiceService } from 'src/app/services/steup-service.service';
declare var introJs: any;
import { LocationStrategy } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, AfterViewInit, OnDestroy {
  token = 'toujeo-179';
  intro: any;
  liveMsg = false;
  getMsg
  receptionEnd = false;
  auditoriumLeft = false;
  helpDesk = false;
  auditoriumRight = false;
  networkingLounge = false;
  exhibitionHall = false;
  showVideo = false;
  relatedPdf;
  pages = [
    { id: 1, path: '/hexagon', analytics: 'click_memorywall', pulseId: 'gameZone'},
    { id: 2, path: '/meetings', analytics: 'click_games', pulseId: 'eventPulse' },
    { id: 7, path: '/photobooth', analytics: 'click_photobooth', pulseId: 'gratitudePulse' },
    { id: 4, path: '/meetings', analytics: 'auditorium_205004', pulseId: 'photobooth' },
    // { id: 6, path: '/support', analytics: 'click_helpdesk', pulseId: 'helpdeskPulse'}
  ];
  allPdfs = [
    // {id:1, pdf:'../../../assets/spmcil/pdf/Standee-03.pdf'},
    // {id:4, pdf:'../../../assets/spmcil/pdf/Standee-04.pdf'},
    // {id:7, pdf:'../../../assets/spmcil/pdf/Standee-02.pdf'},
    // {id:8, pdf:'assets/ipf/images/agenda/agenda.pdf'},
  ];
  popups = [
    // {id:5, datatarget:'feedbackModal', analytics: 'click_agenda', pulseId: 'agendaPulse'},
    { id: 6, datatarget: 'helpDeskModal', analytics: 'click_helpdesk', pulseId: 'helpdeskPulse' },
    // {id:7, datatarget:'speakersModal', analytics: 'click_speakerprofile'},
  ];
  speaker;
  constructor(private chat: ChatService,private router: Router, private _analytics: SteupServiceService, private sanitiser: DomSanitizer,private locationStrategy: LocationStrategy) { }

  ngOnInit(): void {
    history.pushState(null, null, location.href);
    this.locationStrategy.onPopState(() => {
      history.pushState(null, null, location.href);
    })
    
    this.stepup('click_lobby');
    // let videoplay: any = document.getElementById('lobbyBgVideo');
    // videoplay.play();
    // let centerPlay: any = document.getElementById('centerPlay');
    // centerPlay.play();
    this.speaker = JSON.parse(localStorage.getItem('virtual')).category;
    
    // this.getUserguide();
    this.chat.getconnect(this.token);
    this.chat.getMessages().subscribe((data => {
      // console.log('HEILL'+data);
      let check = data.split('_');
      if (check[0] == 'start' && check[1] == 'live') {

        this.getMsg = check[2]
        this.liveMsg = true;
      }
      if (check[0] == 'stop' && check[1] == 'live') {

        this.liveMsg = false;
      }

      // console.log('testing', res);
    }));
  }
  videoPlay(data) {

    if(data == 'click_photo') {
    this.router.navigate(['/photobooth']);

    }
    // alert(data)
    //let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // alert(timer)
    // if (timer >= '18:59:59') {
    if (data === 'click_memorywall') {
      // alert(data)
      // let pauseVideo: any = document.getElementById("bgImg");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
      this.networkingLounge = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'click_games') {
      // alert(data)
      // let pauseVideo: any = document.getElementById("bgImg");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
      this.receptionEnd = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'click_breakout') {
      // alert(data)
      // let pauseVideo: any = document.getElementById("bgImg");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'auditorium_205004') {
      // alert(data)
      // let pauseVideo: any = document.getElementById("bgImg");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'click_photobooth') {
      // alert(data)
      // let pauseVideo: any = document.getElementById("bgImg");
      // pauseVideo.currentTime = 0;
      // pauseVideo.pause();
      this.helpDesk = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }

    // this.receptionEnd = true;
    // this.auditoriumLeft = true;
    // this.auditoriumRight = true;
    // this.networkingLounge = true;
    // this.showVideo = true;
    // let vid: any = document.getElementById(data);
    // vid.play();
    // }else {
    //   $('.note3').modal('show');
    // }

    // let pauseVideo: any = document.getElementById("bhudha");
    //   pauseVideo.currentTime = 0;
    //   pauseVideo.pause();
  }
  skipButton2() {
    this.receptionEnd = false;
    this.router.navigateByUrl('/meetings');
  }
  skipButton3() {
    this.networkingLounge = false;
    this.router.navigateByUrl('/breakout-one');
  }
  skipButton4() {
    this.helpDesk = false;
    this.router.navigateByUrl('/photobooth');
  }
  skipButton5() {
    this.auditoriumLeft = false;
    this.router.navigateByUrl('/eventhall');
  }
  receptionEndVideo() {
    this.router.navigate(['/meetings']);

  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/eventhall']);
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/meetings']);
  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/breakout-one']);
  }
  gotoHelpdeskOnVideoEnd(){
    this.router.navigate(['/photobooth']);
  }
  stepup(action) {
    this._analytics.stepUpAnalytics(action);
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.getUserguide();
    }, 1000);
  }
  getUserguide() {

    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      steps: [
        {
          element: document.querySelectorAll("#helpdeskPulse")[0],
          intro: "<div style='text-align:center'>For any queries relating to the event please visit the tech support</div>"
        },
        {
          element: document.querySelectorAll("#eventPulse")[0],
          intro: "<div style='text-align:center'>Click here to go game zone</div>"
        },
        {
          element: document.querySelectorAll("#gratitudePulse")[0],
          intro: "<div style='text-align:center'>Click here to go photobooth</div>"
        },
        {
          element: document.querySelectorAll("#gameZone")[0],
          intro: "<div style='text-align:center'>Click here to go memory wall</div>"
        },
        {
          element: document.querySelectorAll("#photobooth")[0],
          intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule</div>"
        },

      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");


    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }
  // openPdf(pdf) {
  //   this.relatedPdf = this.sanitiser.bypassSecurityTrustResourceUrl(pdf);
  //   $('#lobbyModal').modal('show');

  // }
  ngOnDestroy() {
    // if (localStorage.getItem('user_guide') === 'start') {
    //   let stop = () => this.intro.exit();
    //   stop();
    // }
    // localStorage.removeItem('user_guide');
  }
}
